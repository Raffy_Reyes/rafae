﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
namespace RafaelReyes_Test.Models
{
    public class User:IDisposable,IValidatableObject
    {

        public string UserID { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "First Name must contain alpha characters only")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Last Name must contain alpha characters only")]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"^[0-9a-zA-Z]+$", ErrorMessage = "Password only accepts alpha numeric input")]
        public string Password { get; set; }
        [Required]
        public string cPassword { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone Number only accepts numeric input")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Enabled { get; set; }
        public string IsAdmin { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public IEnumerable<SelectListItem> AccessLevelList { get { return this.GenerateAccessLevelList(); } }
        public User() {
            this.UserID = null;
            this.Email = null;
            this.FirstName = null;
            this.LastName = null;
            this.Password = null;
            this.PhoneNumber = null;
            this.Address = null;
            this.Notes = null;
            this.BirthDay = null;
            this.Enabled = null;
            this.IsAdmin = null;
            this.CreatedBy = null;
            this.CreatedOn = null;
            this.ModifiedBy=null;
            this.ModifiedOn=null;
            this.cPassword = null;
        }

        private IEnumerable<SelectListItem> GenerateAccessLevelList() {
            List<SelectListItem> lst = new List<SelectListItem>();
            lst.Add(new SelectListItem() { Text = "Administrator", Value = "1" });
            lst.Add(new SelectListItem() { Text = "User", Value = "0" });
            return lst;
        }
        public IEnumerable<User> GetAllActiveUsers()
        {
            List<User> lstUser = new List<User>();
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM dbo.[Users] WHERE enabled='1' ORDER BY IsAdmin DESC,UserID DESC";
                    cn.Open();
                    try
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstUser.Add(new User() {
                                    Address = dr.GetStringOrEmpty("Address"),
                                    BirthDay = dr.GetDateTimeOrNull("BirthDay"),
                                    Email = dr.GetStringOrEmpty("Email"),
                                    Enabled = dr.GetStringOrEmpty("Enabled"),
                                    FirstName = dr.GetStringOrEmpty("FirstName"),
                                    LastName = dr.GetStringOrEmpty("LastName"),
                                    Notes = dr.GetStringOrEmpty("Notes"),
                                    Password = dr.GetStringOrEmpty("Password"),
                                    PhoneNumber = dr.GetStringOrEmpty("PhoneNumber"),
                                    UserID = dr.GetStringOrEmpty("UserID"),
                                    IsAdmin = dr.GetStringOrEmpty("IsAdmin"),
                                    CreatedBy = dr.GetStringOrEmpty("CreatedBy"),
                                    CreatedOn = dr.GetDateTimeOrNull("CreatedOn"),
                                    ModifiedOn = dr.GetDateTimeOrNull("ModifiedOn"),
                                    ModifiedBy = dr.GetStringOrEmpty("ModifiedBy")
                                });
                            }
                        }
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    {
                        cn.Close();
                    }
                }
            }
            return lstUser;
        }
        public void RemoveUser(string UserID)
        {
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE dbo.[Users] SET Enabled='0' WHERE UserID=@UserID";
                    cmd.Parameters.AddWithValue("@UserID",UserID);
                    try
                    {
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    { cn.Close(); }
                }
            }
        }

        public User GetItemsByUserID(string UserID)
        {
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT 
                                        UserID,
                                        [Password],
                                        FirstName,
                                        LastName,
                                        Email,
                                        PhoneNumber,
                                        [Address],
                                        Notes,
                                        BirthDay,
                                        IsAdmin,
                                        [Enabled],
                                        CreatedOn,
                                        (SELECT FirstName+' '+LastName FROM dbo.[Users] WHERE UserID=A.CreatedBy) AS CreatedBy,
                                        ModifiedOn,
                                        (SELECT FirstName+' '+LastName FROM dbo.[Users] WHERE UserID=A.ModifiedBy) AS ModifiedBy
                                        FROM dbo.[Users] AS A
                                        WHERE UserID=@UserID";
                    cmd.Parameters.AddWithValueOrNull("@UserID", UserID);

                    try
                    { 
                        cn.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                return new User()
                                {
                                    Address = dr.GetStringOrEmpty("Address"),
                                    BirthDay = dr.GetDateTimeOrNull("BirthDay"),
                                    Email = dr.GetStringOrEmpty("Email"),
                                    Enabled = dr.GetStringOrEmpty("Enabled"),
                                    FirstName = dr.GetStringOrEmpty("FirstName"),
                                    LastName = dr.GetStringOrEmpty("LastName"),
                                    Notes = dr.GetStringOrEmpty("Notes"),
                                    Password = dr.GetStringOrEmpty("Password"),
                                    PhoneNumber = dr.GetStringOrEmpty("PhoneNumber"),
                                    UserID = dr.GetStringOrEmpty("UserID"),
                                    IsAdmin = dr.GetStringOrEmpty("IsAdmin"),
                                    CreatedBy = dr.GetStringOrEmpty("CreatedBy"),
                                    CreatedOn = dr.GetDateTimeOrNull("CreatedOn"),
                                    ModifiedOn = dr.GetDateTimeOrNull("ModifiedOn"),
                                    ModifiedBy = dr.GetStringOrEmpty("ModifiedBy")
                                };
                            }
                            else
                            { return null; }
                        }
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    { cn.Close(); }
                }
            }
        }
           

        public User GetItemsByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            { 
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT 
                                        UserID,
                                        [Password],
                                        FirstName,
                                        LastName,
                                        Email,
                                        PhoneNumber,
                                        [Address],
                                        Notes,
                                        BirthDay,
                                        IsAdmin,
                                        [Enabled],
                                        CreatedOn,
                                        (SELECT FirstName+' '+LastName FROM dbo.[Users] WHERE UserID=A.CreatedBy) AS CreatedBy,
                                        ModifiedOn,
                                        (SELECT FirstName+' '+LastName FROM dbo.[Users] WHERE UserID=A.ModifiedBy) AS ModifiedBy
                                        FROM dbo.[Users] AS A WHERE Email=@Email";
                    cmd.Parameters.AddWithValue("@Email",UserName);
                    cn.Open();
                    try {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                return new User()
                                {
                                    Address = dr.GetStringOrEmpty("Address"),
                                    BirthDay = dr.GetDateTimeOrNull("BirthDay"),
                                    Email = dr.GetStringOrEmpty("Email"),
                                    Enabled = dr.GetStringOrEmpty("Enabled"),
                                    FirstName = dr.GetStringOrEmpty("FirstName"),
                                    LastName = dr.GetStringOrEmpty("LastName"),
                                    Notes = dr.GetStringOrEmpty("Notes"),
                                    Password = dr.GetStringOrEmpty("Password"),
                                    PhoneNumber = dr.GetStringOrEmpty("PhoneNumber"),
                                    UserID = dr.GetStringOrEmpty("UserID"),
                                    IsAdmin = dr.GetStringOrEmpty("IsAdmin"),
                                    CreatedBy = dr.GetStringOrEmpty("CreatedBy"),
                                    CreatedOn = dr.GetDateTimeOrNull("CreatedOn"),
                                    ModifiedOn = dr.GetDateTimeOrNull("ModifiedOn"),
                                    ModifiedBy = dr.GetStringOrEmpty("ModifiedBy")
                                };
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                    finally 
                    {
                        cn.Close();
                    }
                }
            }
        }

        private string GenerateID()
        {
            string strreturn = "";

            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*)+1 FROM dbo.[Users] WHERE LEFT(UserID,8)=@dt";
                    cmd.Parameters.AddWithValueOrNull("@dt",DateTime.Now.ToString("yyyyMMdd"));
                    int sq = 0;
                    string placeholders="000000";
                    try
                    {
                        cn.Open();
                        sq = (int)cmd.ExecuteScalar();
                        placeholders+=sq.ToString();
                        strreturn = DateTime.Now.ToString("yyyyMMdd") + placeholders.Substring(sq.ToString().Length, 6);
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    {
                        cn.Close();
                    }
                }
            }

            return strreturn;
        }

        public void Insert(User user)
        {
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = @"INSERT INTO dbo.[Users] VALUES(
                                        @UserID,
                                        @Password,
                                        @FirstName,
                                        @LastName,
                                        @Email,
                                        @PhoneNumber,
                                        @Address,
                                        @Notes,
                                        @BirthDay,
                                        @IsAdmin,
                                        @Enabled,
                                        @CreatedOn,
                                        @CreatedBy,
                                        @ModifiedOn,
                                        @ModifiedBy
                                        )";
                    cmd.Parameters.AddWithValueOrNull(@"UserID",this.GenerateID());
                    cmd.Parameters.AddWithValueOrNull(@"Password",user.Password);
                    cmd.Parameters.AddWithValueOrNull(@"FirstName",user.FirstName);
                    cmd.Parameters.AddWithValueOrNull(@"LastName",user.LastName);
                    cmd.Parameters.AddWithValueOrNull(@"Email",user.Email);
                    cmd.Parameters.AddWithValueOrNull(@"PhoneNumber",user.PhoneNumber);
                    cmd.Parameters.AddWithValueOrNull(@"Address",user.Address);
                    cmd.Parameters.AddWithValueOrNull(@"Notes",user.Notes);
                    cmd.Parameters.AddWithValueOrNull(@"BirthDay",(DateTime)user.BirthDay);
                    cmd.Parameters.AddWithValueOrNull(@"IsAdmin",user.IsAdmin);
                    cmd.Parameters.AddWithValueOrNull(@"Enabled","1");
                    cmd.Parameters.AddWithValueOrNull(@"CreatedOn",user.CreatedOn);
                    cmd.Parameters.AddWithValueOrNull(@"CreatedBy",user.CreatedBy);
                    cmd.Parameters.AddWithValueOrNull(@"ModifiedOn",user.ModifiedOn);
                    cmd.Parameters.AddWithValueOrNull(@"ModifiedBy",user.ModifiedBy);

                    try
                    {
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    { cn.Close(); }
                }
            }
        }

        public void Update(User user) 
        {
            using (SqlConnection cn = new SqlConnection(StaticGlobal.ConnString))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandText = @"UPDATE dbo.[Users] SET
                                    [Password]=@Password,
                                    FirstName=@FirstName,
                                    LastName=@LastName,
                                    Email=@Email,
                                    PhoneNumber=@PhoneNumber,
                                    [Address]=@Address,
                                    Notes=@Notes,"
                        +(user.BirthDay!=null?"BirthDay=@BirthDay,":"")+
                                    @"IsAdmin=@IsAdmin,
                                    ModifiedOn=@ModifiedOn,
                                    ModifiedBy=@ModifiedBy
                                    WHERE UserID=@UserID";

                    cmd.Parameters.AddWithValueOrNull("@Password", user.Password);
                    cmd.Parameters.AddWithValueOrNull("@FirstName", user.FirstName);
                    cmd.Parameters.AddWithValueOrNull("@LastName", user.LastName);
                    cmd.Parameters.AddWithValueOrNull("@Email", user.Email);
                    cmd.Parameters.AddWithValueOrNull("@PhoneNumber", user.PhoneNumber);
                    cmd.Parameters.AddWithValueOrNull("@Address", user.Address);
                    cmd.Parameters.AddWithValueOrNull("@Notes", user.Notes);
                    if (user.BirthDay != null)
                    {
                        cmd.Parameters.AddWithValueOrNull("@BirthDay", user.BirthDay);
                    }
                    cmd.Parameters.AddWithValueOrNull("@IsAdmin", user.IsAdmin);
                    cmd.Parameters.AddWithValueOrNull("@ModifiedOn", Convert.ToDateTime(user.ModifiedOn).ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValueOrNull("@ModifiedBy", user.ModifiedBy);
                    cmd.Parameters.AddWithValueOrNull("@UserID", user.UserID);

                    try
                    {
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    { throw new Exception(e.Message); }
                    finally
                    { cn.Close(); }
                }
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> lst = new List<ValidationResult>();

            if (this.cPassword != null)
            {
                if (this.Password.Trim() != this.cPassword.Trim())
                { 
                    lst.Add(new ValidationResult("Please confirm your password"));
                }            
            }

            return lst;
        }
    }
}