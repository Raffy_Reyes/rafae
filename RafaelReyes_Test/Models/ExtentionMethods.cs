﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RafaelReyes_Test.Models
{
    public static class ExtentionMethods
    {
        public static string GetStringOrEmpty(this System.Data.SqlClient.SqlDataReader dr,string ColumnName) {
            return dr.IsDBNull(dr.GetOrdinal(ColumnName)) ? "" : dr.GetString(dr.GetOrdinal(ColumnName));
        }

        public static DateTime? GetDateTimeOrNull(this System.Data.SqlClient.SqlDataReader dr, string ColumnName)
        {
            return dr.IsDBNull(dr.GetOrdinal(ColumnName))?null:(DateTime?)dr.GetDateTime(dr.GetOrdinal(ColumnName));
        }

        public static void AddWithValueOrNull(this System.Data.SqlClient.SqlParameterCollection param, string parameterName, object value)
        {
            param.AddWithValue(parameterName, (value == null ? DBNull.Value : value));
        }
    }
}