﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RafaelReyes_Test.Models
{
    public class LogIn:IDisposable,IValidatableObject
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> result = new List<ValidationResult>();
            User user = new User().GetItemsByUserName(this.UserName);
            if (user == null)
            {
                result.Add(new ValidationResult("Incorrect Password or Invalid User"));            
            }
            else if (user.Password != this.Password)
            {
                result.Add(new ValidationResult("Incorrect Password or Invalid User"));
            }
            return result;

        }
    }
}