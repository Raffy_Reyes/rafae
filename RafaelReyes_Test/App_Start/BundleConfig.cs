﻿using System.Web;
using System.Web.Optimization;

namespace RafaelReyes_Test
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js")
                        .Include("~/Scripts/bootstrap.min.js")
                        .Include("~/Scripts/GlobalJS.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.unobtrusive.min")
                        .Include("~/Scripts/jquery.validate.min"));
            bundles.Add(new StyleBundle("~/bundle/Global").Include("~/Content/bootstrap.min.css")
                .Include("~/Content/Global.css"));
        }
    }
}
