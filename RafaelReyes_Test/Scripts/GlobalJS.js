﻿


$(document).ready(function () {
    var cpass = $(".cpass");
    var pass = $(".pass");
    var numericOnly = $(".numeric-only");
    var alphaNumericOnly = $(".alpha-numeric-only");
    var verticalCenter = $(".vertical-center");
    var alphaOnly = $(".alpha-only");

    if (alphaOnly.length) {
        $(alphaOnly).keypress(function (e) {
            var allowChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var char = String.fromCharCode(e.which).toUpperCase();
            if (allowChar.lastIndexOf(char) < 0) {
                e.preventDefault();
            }
        });
    }

    if (numericOnly.length)
    {
        $(numericOnly).keypress(function (e) {
            var allowChar = "0123456789";
            var char = String.fromCharCode(e.which).toUpperCase();
            if (allowChar.lastIndexOf(char) < 0)
            {
                e.preventDefault();
            }
        });
    }

    if (alphaNumericOnly.length) {
        $(alphaNumericOnly).keypress(function (e) {
            var allowChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var char = String.fromCharCode(e.which).toUpperCase();
            if (allowChar.lastIndexOf(char) < 0) {
                e.preventDefault();
            }
        });
    }

    if (verticalCenter.length) {
        var height = $(verticalCenter).outerHeight() / 2;
        $(verticalCenter).css({ marginTop: (height * -1) + "px" });
    }
    $("form").on("submit", function (e) {

        if (cpass.length && pass.length) {
            if ($(pass).val().trim() !== $(cpass).val().trim()) {

                e.preventDefault();
                $("[data-valmsg-summary='true'] ul").html("<li>Please confirm your password</li>");
                $("[data-valmsg-summary='true']").attr("class", "validation-summary-errors");
                return false;
            }
        }


        return true;

    });


});