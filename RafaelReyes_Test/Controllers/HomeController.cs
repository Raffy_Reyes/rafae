﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RafaelReyes_Test.Models;
using System.Web.Security;
namespace RafaelReyes_Test.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            FormsAuthentication.SignOut();
            return View();
        }

        [HttpPost]
        public ActionResult Index(LogIn model)
        {
            if (ModelState.IsValid)
            {
                User user = new User().GetItemsByUserName(model.UserName);
                FormsAuthentication.SetAuthCookie(user.UserID, true);
                Session["UserName"] = user.FirstName + " " + user.LastName;
                Session["IsAdmin"] = user.IsAdmin;
                if (user.IsAdmin == "1")
                {
                    return RedirectToAction("UserList", "Admin");
                }
                    return RedirectToAction("MyAccount", "User");                
            }
            return View(model);
        }
	}
}