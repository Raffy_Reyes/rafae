﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RafaelReyes_Test.Models;
using PagedList;
using PagedList.Mvc;
using System.Web.Security;
namespace RafaelReyes_Test.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        [HttpGet]
        public ActionResult UserList(int? page, string searchValue="")
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
            var UserID = ticket.Name;
            if (Session["IsAdmin"].ToString().Trim() == "1")
            {
                return View(new User().GetAllActiveUsers().Where(x => (x.FirstName + x.LastName + x.Email).ToUpper().Contains(searchValue.ToUpper()) && x.UserID != UserID).ToPagedList(page ?? 1, 20));
            }
                return RedirectToAction("MyAccount", "user");
        }

        public ActionResult RemoveUser(string ID)
        {
            if (Session["IsAdmin"].ToString().Trim() == "1")
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                var UserID = ticket.Name;
                User user = new User();
                user.RemoveUser(ID);
                return View("UserList", user.GetAllActiveUsers().Where(x => x.UserID != UserID).ToPagedList(1, 20));            
            }
            return RedirectToAction("MyAccount", "user");
        }


        public ActionResult EditUser(string ID) 
        {
            if (Session["IsAdmin"].ToString().Trim() == "1") 
            {
                User model = new User().GetItemsByUserID(ID);
                return View(model);            
            }
            return RedirectToAction("MyAccount", "user");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult EditUser(User model)
        {
            if (Session["IsAdmin"].ToString().Trim() == "1")
            {
                if (ModelState.IsValid)
                {
                    HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    var UserID = ticket.Name;
                    model.ModifiedBy = UserID;
                    model.ModifiedOn = DateTime.Now;
                    new User().Update(model);
                    return RedirectToAction("UserList", "Admin");
                }
                return View(model);
            }
            return RedirectToAction("MyAccount", "user");
        }

        public ActionResult CreateAccount()
        {
            if (Session["IsAdmin"].ToString().Trim() == "1")
            {
                return View(new User() { IsAdmin = "1" });
            } 
            return RedirectToAction("MyAccount", "user");
        }

        [HttpPost]
        public ActionResult CreateAccount(User model) 
        {
            if (Session["IsAdmin"].ToString().Trim() == "1")
            {
                if (ModelState.IsValid)
                {
                    HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    DateTime dt = DateTime.Now;
                    var UserID = ticket.Name;
                    model.CreatedBy = UserID;
                    model.CreatedOn = dt;
                    model.ModifiedBy = UserID;
                    model.ModifiedOn = dt;
                    new User().Insert(model);
                    return RedirectToAction("UserList", "Admin");
                }

                return View(model);            
            }
            return RedirectToAction("MyAccount", "user");
        }

        
	}
}