﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using RafaelReyes_Test.Models;
namespace RafaelReyes_Test.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult MyAccount()
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
            var UserID = ticket.Name;
            return View(new User().GetItemsByUserID(UserID));
        }

        [HttpPost]
        public ActionResult MyAccount(User model)
        {
            if (ModelState.IsValid)
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                var UserID = ticket.Name;
                model.ModifiedBy = UserID;
                model.ModifiedOn = DateTime.Now;
                new User().Update(model);

                Session["UserName"] = model.FirstName + " " + model.LastName;
                if (model.IsAdmin == "1")
                {
                    return RedirectToAction("UserList", "Admin");
                }
            }
            return View(model);
        }
	}
}