﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RafaelReyes_Test.Startup))]
namespace RafaelReyes_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
